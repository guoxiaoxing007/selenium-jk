from testcases import testcase1, testcase2
from util import util
from selenium import webdriver
from testcases.basic.test_user_register import TestUserRegister
from testcases.basic.test_user_login import TestUserLogin
from testcases.basic.test_admin_login import TestAdminLogin
from testcases.basic.test_category import TestCategory
from testcases.basic.test_article import TestArticle

if __name__ == '__main__':
    # testcase1.test1()
    # testcase1.test2()
    # testcase2.test1()
    # testcase2.test2()
    # print(util.gen_random_str())

    # driver = webdriver.Chrome()
    # driver.get('http://localhost:8080/user/register')
    # driver.maximize_window()
    #
    # print(util.get_code(driver, 'captcha-img'))、

    # 注册用例
    # case01 = TestUserRegister()
    # case01.test_register_code_error()
    # case01.test_register_ok()

    # 登录用例
    # case02 = TestUserLogin()
    # case02.test_user_login_username_error()
    # case02.test_user_login_ok()

    # 测试管理员登录
    # case03 = TestAdminLogin()
    # case03.test_admin_login_code_error()
    # case03.test_admin_login_code_ok()

    # 测试文章分类
    # login = TestAdminLogin()
    # login.test_admin_login_code_ok()
    # case04 = TestCategory(login)
    # case04.test_add_category_error()
    # case04.test_add_category_ok()

    # 测试文章添加和删除
    login = TestAdminLogin()
    login.test_admin_login_code_ok()
    case05 = TestArticle(login)
    case05.test_add_ok()
    case05.test_delete_one_article_ok()
    # case05.test_delete_all_article_ok()
