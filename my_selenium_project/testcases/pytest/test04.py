import pytest


# 使用pytest.mark在函数上标记
@pytest.mark.do
def test01():
    print('test01')


@pytest.mark.undo
def test02():
    print('test02')
