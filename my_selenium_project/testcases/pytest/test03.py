import pytest


# 显式指定函数名 用::   pytest test03.py::test1
# 模糊匹配 用-k      pytest -k test test03.py
#
def test1():
    print('test01')


def test2():
    print('test02')
