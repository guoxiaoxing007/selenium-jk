import pytest


class TestLoginCase(object):
    def test01(self):
        print('test01')
        assert 1 == 1

    def test02(self):
        print('test02')
        assert 1 == 2


if __name__ == '__main__':
    # -s 显示测试函数中print()函数输出
    # -v 显示每个测试函数的执行结果
    pytest.main(['-s', '-v', 'test_01.py'])
