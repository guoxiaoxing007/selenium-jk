import unittest
from selenium import webdriver
from time import sleep


class MyTestCase2(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print('setUpClass...')
        cls.driver = webdriver.Chrome()
        cls.driver.get('http://www.baidu.com')
        cls.driver.maximize_window()

    # def setUp(self) -> None:
    #     print('setup...')
    #
    # def tearDown(self) -> None:
    #     print('tearDown...')

    def test01(self):
        self.driver.find_element_by_id('kw').send_keys('bala')
        print('test01')
        sleep(2)

    def test02(self):
        print('test02')
        self.assertEqual(1, 1)
        self.assertIn(10, [1, 2, 3])

    @classmethod
    def tearDownClass(cls) -> None:
        print('tearDownClass...')
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()
