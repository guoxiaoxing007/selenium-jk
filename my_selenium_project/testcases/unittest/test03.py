import unittest
import os


class MyTestCase03(unittest.TestCase):

    def test01(self):
        print('test01')

    def test02(self):
        print('test02')


class MyTestCase04(unittest.TestCase):

    def test03(self):
        print('test03')

    def test04(self):
        print('test04')


if __name__ == '__main__':
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()

    # 方法一 通过测试用例类进行加载
    # suite.addTest(loader.loadTestsFromTestCase(MyTestCase03))
    # suite.addTest(loader.loadTestsFromTestCase(MyTestCase04))

    # 方法二 通过测试用例模板进行加载
    # suite.addTest(loader.loadTestsFromModule(MyTestCase03))
    # suite.addTest(loader.loadTestsFromModule(MyTestCase04))

    # 方法三 通过路径进行加载 推荐
    path = os.path.dirname(os.path.abspath(__file__))
    suite.addTest(loader.discover(path))

    # 方法四 逐条加载测试用例  不推荐
    # case1 = MyTestCase03("test01")
    # case2 = MyTestCase03("test02")
    # case3 = MyTestCase04("test03")
    # case4 = MyTestCase04("test04")
    #
    # suite.addTest(case1)
    # suite.addTest(case2)
    # suite.addTest(case3)
    # suite.addTest(case4)

    runner = unittest.TextTestRunner()
    runner.run(suite)
