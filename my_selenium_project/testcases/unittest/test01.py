import unittest


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        print('setup....')

    def test01(self):
        print('test01')
        self.assertEqual(1 + 2, 3)

    def test02(self):
        print('test02')
        self.assertGreaterEqual(5, 4)

    # 必须以test开头，否则不会执行
    def aaa(self):
        print('aaa')

    def tearDown(self) -> None:
        print('tearDown....')


if __name__ == '__main__':
    unittest.main()
