import os

from selenium import webdriver
from time import sleep

from selenium.webdriver.support.select import Select


class TestCase(object):
    def __init__(self):
        self.driver = webdriver.Chrome()
        path = os.path.dirname(os.path.abspath(__file__))
        file_path = 'file:///' + path + '/forms3.html'
        self.driver.get(file_path)

    def test_select(self):
        se = self.driver.find_element_by_id('province')
        select = Select(se)
        # select.select_by_index(2)
        #
        # sleep(2)
        #
        # select.select_by_value('bj')
        #
        # sleep(2)
        #
        # select.select_by_visible_text('BeiJing')
        #
        # sleep(2)

        # # 多选
        # for i in range(3):
        #     select.select_by_index(i)
        #     sleep(1)
        # sleep(3)
        #
        # # 反选
        # select.deselect_all()
        #
        # sleep(3)

        for option in select.options:
            print(option.text)

        self.driver.quit()


if __name__ == '__main__':
    case = TestCase()
    case.test_select()
