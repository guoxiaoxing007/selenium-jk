from selenium import webdriver
from time import sleep, strftime, localtime, time
import os


class TestCase(object):
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.get('http://www.baidu.com')

    def test1(self):
        self.driver.find_element_by_id('kw').send_keys('selenium')
        self.driver.find_element_by_id('su').click()

        sleep(2)

        # st = strftime("%Y-%m-%d-%H-%M-%S", localtime(time()))
        # file_name = st + '.png'
        # self.driver.save_screenshot(file_name)

        st = strftime("%Y-%m-%d-%H-%M-%S", localtime(time()))
        file_name = st + '.png'

        path = os.path.abspath('screenshot')
        print(path)
        file_path = path + '/' + file_name
        print(file_path)
        self.driver.get_screenshot_as_file(file_path)

        sleep(2)


if __name__ == '__main__':
    case = TestCase()
    case.test1()
    case.driver.quit()
