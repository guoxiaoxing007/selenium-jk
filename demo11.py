from selenium import webdriver
from time import sleep

from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


class TestCase(object):
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()

    def test_mouse(self):
        self.driver.get('https://sahitest.com/demo/clicks.htm')

        # double_click 鼠标双击
        btn = self.driver.find_element_by_xpath('/html/body/form/input[2]')
        ActionChains(self.driver).double_click(btn).perform()

        sleep(2)

        # click 鼠标单击
        btn = self.driver.find_element_by_xpath('/html/body/form/input[3]')
        ActionChains(self.driver).click(btn).perform()

        sleep(2)

        # context_click 鼠标右击
        btn = self.driver.find_element_by_xpath('/html/body/form/input[4]')
        ActionChains(self.driver).context_click(btn).perform()

        sleep(5)

    def test_key(self):
        self.driver.get('http://www.baidu.com')
        kw = self.driver.find_element_by_id('kw')
        kw.send_keys('selenium')
        kw.send_keys(Keys.COMMAND, 'a')
        sleep(2)
        kw.send_keys(Keys.COMMAND, 'x')
        sleep(2)
        kw.send_keys(Keys.COMMAND, 'v')
        sleep(2)
        link = self.driver.find_element_by_link_text('百度首页')
        ActionChains(self.driver).move_to_element(link).click(link).perform()
        sleep(3)


if __name__ == '__main__':
    case = TestCase()
    # case.test_mouse()
    case.test_key()
    case.driver.quit()
